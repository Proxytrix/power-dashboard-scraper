# Setup

* Install node.js 14 or later https://nodejs.org/en/download/
* Install yarn by running

```
npm i -g yarn
```

* Copy config-example.json to config.json and add your information to config.json

* Run the following command to install dependencies:

```
yarn
```

# Run
```
yarn start
```
