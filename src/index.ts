import puppeteer from "puppeteer-extra";
import stealthPlugin from "puppeteer-extra-plugin-stealth";
import {executablePath, HTTPResponse} from "puppeteer";
import moment from "moment-timezone";
import fs from "fs-extra";
import Bluebird from "bluebird";
import {Config, SSchwazStatisticsResponse} from "./types";
import {join} from "path";

const config: Config = require('../config.json');

async function main() {
    puppeteer.use(stealthPlugin());

    const browser = await puppeteer.launch({
        executablePath: executablePath(),
        headless: false,
        slowMo: 35
    });

    const page = await browser.newPage();

    await page.goto(config.loginUrl);

    // click away cookie consent
    await page.waitForSelector('.cc_btn_accept_all');
    await page.click('.cc_btn_accept_all');

    // login
    await page.waitForSelector('#form_username');
    await page.type('#form_username', config.username);
    await page.type('#form_password', config.password);
    await page.click('#form_send');

    // extract customer number (not needed but I display it in the console)
    await page.waitForSelector('.CustomerNo');
    const customerNumberElement = await page.$('.CustomerNo')
    const customerNumber = await page.evaluate(el => el.textContent, customerNumberElement);

    console.log(`logged in as ${customerNumber}`);

    // prepare interception callback
    let interceptedResponse: SSchwazStatisticsResponse;
    async function onResponse(response: HTTPResponse) {
        const url = response.url();
        if (url.indexOf('ajaxService=getLoadProfile') > -1) {
            console.log(`intercepted ${url}`);
            const res = await response.text();
            if(res && res.indexOf('[') === 0) {
                console.log('this is a json');
                interceptedResponse = await response.json();
            } else {
                console.log('this is not a json');
                if(res) {
                    console.log('we actually got ' + res.substr(0, 30));
                }
            }
        }
    }


    // add interception handler, navigate to the statistics page and wait for
    // the graph to appear
    await page.waitForSelector('[data-loadpage="loadprofile"]');
    await Bluebird.delay(6000);
    page.on('response', onResponse);
    await page.click('[data-loadpage="loadprofile"]');
    await page.waitForSelector('canvas.flot-overlay');
    page.off('response', onResponse);


    // map and save
    const mappedResponse = interceptedResponse[0].data.map(r => {
        return {
            date: moment.tz(r[0], 'Europe/Vienna')
                .subtract(1, 'day') // stadtwerke have the wrong date (1 day ahead), here we correct for it
                .format('YYYY-MM-DD'),
            usageKWh: r[1]
        };
    })
    await fs.writeFile(join(__dirname, '../reports/usage.json'), JSON.stringify(mappedResponse, null, 2));
    await fs.writeFile(join(__dirname, '../reports/usage-raw.json'), JSON.stringify(interceptedResponse, null, 2));


    await Bluebird.delay(5000);
    // cleanp
    await page.close();
    await browser.close();
}

main();
