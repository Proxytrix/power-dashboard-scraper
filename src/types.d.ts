
export interface SSchwazStatisticsResponse extends Array<SSchwazStatistic> {

}

export interface SSchwazStatistic {
    period: number
    type: string
    devid: string
    scaid: string
    color: string
    data: (number | undefined)[][]
}


export interface Config {
    username: string;
    password: string;
    loginUrl: string;
}